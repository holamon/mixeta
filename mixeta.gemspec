Gem::Specification.new do |s|
  # Release Specific Information
  s.version = "1.0"
  s.date = "2013-12-05"

  # Gem Details
  s.name = "mixeta"
  s.authors = ["@holamon"]
  s.summary = %q{my collection of mixins}
  s.description = %q{my collection of mixins}

  # Gem Files
  s.files = %w(README.md)
  s.files += Dir.glob("lib/**/*.*")
  s.files += Dir.glob("stylesheets/**/*.*")
  s.files += Dir.glob("templates/**/*.*")

  # Gem Bookkeeping
  s.rubygems_version = %q{2.0.3}
  s.add_dependency("compass", [">= 1.0.1"])
  s.add_development_dependency("hologram")

end